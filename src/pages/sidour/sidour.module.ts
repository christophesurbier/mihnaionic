import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SidourPage } from './sidour';

@NgModule({
  declarations: [
    SidourPage,
  ],
  imports: [
    IonicPageModule.forChild(SidourPage),
  ],
})
export class SidourPageModule {}
