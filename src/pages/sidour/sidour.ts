import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as Constant from '../../config/constants';
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';

/**
 * Generated class for the SidourPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sidour',
  templateUrl: 'sidour.html',
})
export class SidourPage {
  phonetique: boolean = true;
  textButton = "Hébreu";
  titleContent = "Edot HaMizrah phonétique"
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no'
    hideurlbar:'yes',
    hidenavigationbuttons:'no', 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'no',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Fermer', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'no', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only 
    useWideViewPort:'no'   
};
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public apiService: ApiserviceProvider,
    private inAppBrowser: InAppBrowser) {
  }

  displayChoices() {

  }
  changePreference() {
    this.phonetique = !this.phonetique
    console.log("phonetique " + this.phonetique)
    if (this.phonetique) {
      this.textButton = "Hébreu"
      this.titleContent = "Edot HaMizrah phonétique"
    }
    else {
      this.titleContent = "Edot HaMizrah"
      this.textButton = "Phonétique"
    }
    let valeur = ""
    if (this.phonetique) {
      valeur = "1"
    }
    else {
      valeur = "0"
    }
    this.storage.set(Constant.domainConfig.client + '_sidourpreference', valeur)
    this.displayChoices()
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad SidourPage');
    this.storage.get(Constant.domainConfig.client + '_sidourpreference').then((preference) => {
      if (preference) {
        if (preference == "1") {
          this.phonetique = true
        }
        else {
          this.phonetique = false
        }
        console.log("on lit preference " + this.phonetique)
        if (this.phonetique) {
          this.textButton = "Hébreu"
          this.titleContent = "Edot HaMizrah phonétique"
        }
        else {
          this.titleContent = "Edot HaMizrah"
          this.textButton = "Phonétique"
        }
      }
      else {
        console.log("Pas de preference")
      }

    });
  }

  showYoutube() {
    let url = "https://www.youtube.com/watch?v=NP63wayuR2A"
    const options: InAppBrowserOptions = {
      zoom: 'no'
    }

    // Opening a URL and returning an InAppBrowserObject
    const browser = this.inAppBrowser.create(url, '_system', options);
  }
  showPriere(titre, numero) {
    console.log("on doit montrer " + titre)
    if (this.apiService.networkConnected) {


      let file = ""
      if (numero == 1) {
        file = "BenedictionsduMatin.htm"
      }
      else if (numero == 2) {
        file = "AcherYatsarphonetique.htm"
      }
      else if (numero == 3) {
        file = "Chemaphonetique.htm"
      }
      else if (numero == 4) {
        file = "amidaphonetique.htm"
      }
      else if (numero == 5) {
        file = "Chaharitphonetique.htm"
      }
      else if (numero == 6) {
        file = "Minhaphonetique.htm"
      }
      else if (numero == 7) {
        file = "Arvitphonetique.htm"
      }
      else if (numero == 8) {
        file = "BirkatHamazonev5.htm"
      }
      else if (numero == 9) {
        titre = "Mé'ène Chaloche phonétique"
        file = "MeeneChalochphonetique.htm"
      }
      else if (numero == 10) {
        file = "KADDISH.htm"
      }
      else if (numero == 11) {
        file = "BIRKATALEVANAPHONETIQUE.htm"
      }
      else if (numero == 12) {
        file = "Divers.htm"
      }
      else if (numero == 13) {
        file = "BircatHahilanotPhonetique.htm"
      }
      
      let url ="https://minhaparis.idevotion.fr/minhaparis/prierehtml/"+file
      let target="_self"
      this.inAppBrowser.create(url,target,this.options)
     /*
      this.navCtrl.push("ShowPrierePage", {
        titre: titre,
        file: file
      })
   */
    }
    else {
      this.apiService.showNoNetwork()
    }

  }

  showPriereHebreu(numero) {

    if (this.apiService.networkConnected) {


      let titre = ""
      let file = ""
      if (numero == 1) {
        titre = "Bérakhot du matin ברכות השחר"
        file = "bircathachahar.htm"
      }
      else if (numero == 2) {
        file = "chaharit.htm"
        titre = "Chaharit שחרית"
      }
      else if (numero == 3) {
        file = "Minha.htm"
        titre = "Minha מנחה"
      }
      else if (numero == 4) {
        file = "Arvit.htm"
        titre = "Arvith ערבית"
      }
      else if (numero == 5) {
        file = "chemaalamita.htm"
        titre = "Au coucher קריאת שמע על המיטה"
      }
      else if (numero == 6) {
        file = "BirkatHamazone.htm"
        titre = "Birkat Hamazone ברכת המזוֹן"
      }
      else if (numero == 7) {
        file = "Meenechaloche.htm"
        titre = "Mé'ène Chaloche מעין שלוֹש"
      }
      else if (numero == 8) {
        file = "AcherYatsar.htm"
        titre = "Achère Yatsar אשר יצר"
      }
      else if (numero == 9) {
        titre = "Birkat Halévana קידוש הלבנה"
        file = "BirkatAlenava.htm"
      }
      else if (numero == 10) {
        file = "Tefilataderekh.htm"
        titre = "Avant un voyage תפלת הדרך"
      }
      else if (numero == 11) {
        file = "ChevaBerakhot.htm"
        titre = "Chévah Bérakhot שבע ברכות"
      }
      else if (numero == 12) {
        titre = "Hachkaba השכבה"
        file = "hachkaba.htm"
      }
     
      let url ="https://minhaparis.idevotion.fr/minhaparis/prierehtml/"+file
      let target="_self"
      this.inAppBrowser.create(url,target,this.options)
      /*
      this.navCtrl.push("ShowPrierePage", {
        titre: titre,
        file: file
      })
      */
    }
    else {
      this.apiService.showNoNetwork()
    }
  }
}
