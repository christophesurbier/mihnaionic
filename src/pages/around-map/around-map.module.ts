import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AroundMapPage } from './around-map';

@NgModule({
  declarations: [
    AroundMapPage,
  ],
  imports: [
    IonicPageModule.forChild(AroundMapPage),
  ],
})
export class AroundMapPageModule {}
