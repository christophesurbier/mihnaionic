import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';

/**
 * Generated class for the VillePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ville',
  templateUrl: 'ville.html',
})
export class VillePage {
  tabBarElement: any;
  onlyRP : boolean;
  data : any;
  keys : any;
  dico : any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
  public apiService : ApiserviceProvider) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    this.onlyRP = this.navParams.get("onlyRP")
    this.loadData();
  }

  
  loadData(){
    if (this.apiService.networkConnected){
      this.apiService.showLoading()
      this.apiService.getVilleJson(this.onlyRP).subscribe(list=>{
        this.apiService.stopLoading()
        if (list){
          this.data = list;
          this.dico ={}
          for (var i = 0; i < this.data.length; i += 1) {
            // Use the index i here
            let ville = this.data[i]["ville"]
            let key = ville.charAt(0);
             if (this.dico[key]){
              let existingArray = this.dico[key]
            
              existingArray.push(ville)
              this.dico[key]= existingArray 
            }
            else {
              // create array with key
              let array = []
              array.push(ville)
              this.dico[key] =  array
            }
          
          }
          this.keys = Object.keys(this.dico)
        }
        else{
          this.apiService.showError("Désolé aucun résultat autour de vous")
        }
      })
    }
    else{
      this.apiService.showNoNetwork()
    }
  } 

  showVille(ville){
    this.navCtrl.push("AroundPage",{
      ville:ville
    })
  }
  
  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }
 
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VillePage');
  }

}
