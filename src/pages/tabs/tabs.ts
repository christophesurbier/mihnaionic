import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular';
@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = "MinhaAParisPage";
  tab2Root = "SidourPage"; 
  tab3Root = "HorairesPage";
  
  constructor() {

  }
}
