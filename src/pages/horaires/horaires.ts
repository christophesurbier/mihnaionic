import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as papa from 'papaparse';
import * as moment from 'moment';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Http } from '@angular/http';

/**
 * Generated class for the HorairesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-horaires',
  templateUrl: 'horaires.html',
})
export class HorairesPage {
  listeHoraire: any;
  bkAubeHidden: boolean;
  bkAubeText: string;
  horaireBkAubeText: string;
  horaireBkAubeTextColor: string;
  horaireBkAubeHidden: boolean;
  luneAubeHidden: boolean;
  fondBkAubeHidden: boolean;
  bkCoucherHidden: boolean;
  horaireBkCoucherHidden: boolean;
  luneCoucherHidden: boolean;
  fondBKCoucherHidden: boolean;
  hautParleurHidden: boolean;
  bkCoucherText: string;
  horaireBkCoucherText: string;
  horaireBkCoucherTextColor: string;
  date: string;
  imageFond: string = ""
  dateHebreu: string;
  aubeTime: any;
  leverTime: any;
  heure1Time: any;
  plagTime: any;
  sortieTime: any;
  debutTime: any;
  coucherTime: any;
  imageLune: string;
  currentDate: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public apiService: ApiserviceProvider,
    private http: Http) {

  }

  ionViewWillEnter() {
    this.readCsvData();
  }

  cacheBK() {
    this.bkAubeHidden = true;
    this.horaireBkAubeHidden = true;
    this.luneAubeHidden = true;
    this.fondBkAubeHidden = true;
    this.bkCoucherHidden = true;
    this.horaireBkCoucherHidden = true;
    this.luneCoucherHidden = true;
    this.fondBKCoucherHidden = true;
  }

  private readCsvData() {

    this.http.get('assets/horaires2013.csv')
      .subscribe(
        data => this.extractData(data),
        err => {
          console.log("Err ")
        }
      );
  }

  toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }
  private extractData(res) {
    let csvData = res['_body'] || '';
    let parsedData = papa.parse(csvData, {
      dynamicTyping: true,
      delimiter: ";",	// auto-detect
    }).data;

    this.listeHoraire = []
    parsedData.forEach((element, idx) => {
      let annee = element[0]
      let aube = element[1]
      let ba = element[2]
      let coucher = element[3]
      let couleur = element[4]
      let debut = element[5]
      let hebreu = element[6]
      let heure1 = element[7]
      let jour = element[8]
      let jourFormate = element[9]
      let lever = element[10]
      let lune = element[11]
      let plag = element[12]
      let sortie = element[13]

      let dataDictionary = {}
      dataDictionary["annee"] = annee;
      dataDictionary["aube"] = aube;
      dataDictionary["ba"] = ba;
      dataDictionary["coucher"] = coucher;
      dataDictionary["couleur"] = couleur;
      dataDictionary["debut"] = debut;
      dataDictionary["hebreu"] = hebreu;
      dataDictionary["heure1"] = heure1;
      dataDictionary["jour"] = jour;
      dataDictionary["jourFormate"] = jourFormate;
      dataDictionary["lever"] = lever;
      dataDictionary["plag"] = plag;
      dataDictionary["sortie"] = sortie;
      dataDictionary["lune"] = lune;

      this.listeHoraire.push(dataDictionary)
    })
    // get week day 
    moment.locale("fr");
    let now = moment()
    this.date = now.format("dddd D MMMM YYYY")
    this.date = this.toTitleCase(this.date)
    // Cherche la date dans le CSV
    this.displayInfo(now)
  }

  precedent() {
    // enleve 1 jour
    this.currentDate = this.currentDate.add(-1, "days")
    this.date = this.currentDate.format("dddd D MMMM YYYY")
    this.date = this.toTitleCase(this.date)
    this.displayInfo(this.currentDate)
  }

  suivant() {
    //Ajoute 1 jour
    this.currentDate = this.currentDate.add(+1, "days")

    this.date = this.currentDate.format("dddd D MMMM YYYY")
    this.date = this.toTitleCase(this.date)
    this.displayInfo(this.currentDate)
  }
  displayInfo(date) {
    this.currentDate = date
    let dateToFind = date.format("DD") + "/" + date.format("MM") + "/" + date.format("YYYY")

    console.log("on cherche " + dateToFind)
    let dicoVoulu = []
    for (let dico of this.listeHoraire) {
      // console.log("on a dico "+JSON.stringify(dico))
      let jourFormate = dico["jourFormate"]
      if (jourFormate == dateToFind) {
        dicoVoulu = dico
        console.log("ok on a dico " + JSON.stringify(dicoVoulu))
        break
      }
    }
    this.dateHebreu = dicoVoulu["hebreu"]
    //Trouve image de fond
    //get current Hour
    // de aube a lever : aube.jpg
    // de lever a heure1 : matin.jpg
    // de heure1 a plag : debut am
    // de plag a sortie : am
    // nuit
    let currentTime = moment(date, "kk:mm").format("kk:mm")
    this.aubeTime = moment(dicoVoulu["aube"], "kk:mm").format("kk:mm")
    this.leverTime = moment(dicoVoulu["lever"], "kk:mm").format("kk:mm")
    this.heure1Time = moment(dicoVoulu["heure1"], "kk:mm").format("kk:mm")
    this.plagTime = moment(dicoVoulu["plag"], "kk:mm").format("kk:mm")
    this.sortieTime = moment(dicoVoulu["sortie"], "kk:mm").format("kk:mm")
    this.debutTime = moment(dicoVoulu["debut"], "kk:mm").format("kk:mm")
    this.coucherTime = moment(dicoVoulu["coucher"], "kk:mm").format("kk:mm")


    if ((currentTime >= this.aubeTime) && (currentTime < this.leverTime)) {
      this.imageFond = "url('assets/imgs/aube.jpg')"
      console.log("Entre aube et lever : aube")
    }
    else if ((currentTime >= this.leverTime) && (currentTime < this.heure1Time)) {
      this.imageFond = "url('assets/imgs/matin.jpg')"
      console.log("Entre lever et heure1 : matin")

    }
    else if ((currentTime >= this.heure1Time) && (currentTime < this.plagTime)) {
      this.imageFond = "url('assets/imgs/debut am.jpg')"
      console.log("Entre heure1 et plag : debut am")

    }
    else if ((currentTime >= this.plagTime) && (currentTime < this.sortieTime)) {
      this.imageFond = "url('assets/imgs/am.jpg')"
      console.log("Entre plag et sortie : am")

    }
    else {
      this.imageFond = "url('assets/imgs/nuit.jpg')"
    }
    this.hautParleurHidden = true;

    this.displayBK(dicoVoulu, dateToFind)
  }
  displayBK(dicoVoulu, dateFormate) {
    let couleur = dicoVoulu["couleur"]
    let lune = dicoVoulu["lune"].toUpperCase()
    lune = lune.replace("LUNE", "moon")
    lune = lune.replace("GIF", "png")

    let alevana = dicoVoulu["ba"]
    console.log("alevana " + alevana)
    if (alevana == "00:00") {
      if (couleur == "R") {
        console.log("Alevana 0")
        this.horaireBkCoucherText = "✖"
        this.horaireBkCoucherTextColor = "red"
      }
      else {
        this.horaireBkCoucherText = "\u2714"
        this.horaireBkCoucherTextColor = "green"
      }

      this.bkCoucherHidden = false;
      this.horaireBkCoucherHidden = false;
      this.luneCoucherHidden = false;
      this.fondBKCoucherHidden = false;
      this.bkAubeHidden = true;
      this.horaireBkAubeHidden = true;
      this.luneAubeHidden = true;
      this.fondBkAubeHidden = true;
      this.bkCoucherText = "Birkat Alévana"
      this.hautParleurHidden = true;
      this.imageLune = "url('assets/imgs/" + lune + "')"

    }
    else {
      //horaires
      let tokens = alevana.split("h")
      let firstToken = tokens[0]
      console.log("firstToken " + firstToken)

      if (firstToken < 8) {
        //Aube
        this.bkCoucherHidden = true;
        this.horaireBkCoucherHidden = true;
        this.luneCoucherHidden = true;
        this.fondBKCoucherHidden = true;
        this.bkAubeHidden = false;
        this.horaireBkAubeHidden = false;
        this.luneAubeHidden = false;
        this.fondBkAubeHidden = false;
        this.bkAubeText = "Birkat Alévana"
        this.horaireBkAubeText = alevana;
        if (couleur == "R") {
          this.horaireBkAubeTextColor = "red"
        }
        else {
          this.horaireBkAubeTextColor = "green"
        }
        //this.checkHautParleur(dateFormate)
        this.imageLune = "url('assets/imgs/" + lune + "')"
      }
      else {
        //Coucher
        this.bkCoucherHidden = false;
        this.horaireBkCoucherHidden = false;
        this.luneCoucherHidden = false;
        this.fondBKCoucherHidden = false;
        this.bkAubeHidden = true;
        this.horaireBkAubeHidden = true;
        this.luneAubeHidden = true;
        this.fondBkAubeHidden = true;
        this.bkCoucherText = "Birkat Alévana"
        this.horaireBkCoucherText = alevana;
        if (couleur == "R") {
          this.horaireBkCoucherTextColor = "red"
        }
        else {
          this.horaireBkCoucherTextColor = "green"
        }
        console.log("Textcolor " + this.horaireBkCoucherTextColor)
        //this.checkHautParleur(dateFormate)
        this.imageLune = "url('assets/imgs/" + lune + "')"
      }
    }
  }

  checkHautParleur(date) {
    let now = moment()

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HorairesPage');
  }

}
