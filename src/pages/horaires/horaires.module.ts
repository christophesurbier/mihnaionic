import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HorairesPage } from './horaires';

@NgModule({
  declarations: [
    HorairesPage,
  ],
  imports: [
    IonicPageModule.forChild(HorairesPage),
  ],
})
export class HorairesPageModule {}
