import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowPrierePage } from './show-priere';
@NgModule({
  declarations: [
    ShowPrierePage,
  ],
  imports: [
    IonicPageModule.forChild(ShowPrierePage),
  ],
})
export class ShowPrierePageModule {}
