import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { InAppBrowser,InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the ShowPrierePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-priere',
  templateUrl: 'show-priere.html',
})

export class ShowPrierePage {
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};
  titre : string;
  file:string;
  safeUrl:any;
  fontSize = 1.33;
  priere:any;
  constructor(public navCtrl: NavController, 
  public navParams: NavParams,
  private sanitizer: DomSanitizer,
  public apiService:ApiserviceProvider,
  public iab: InAppBrowser,
  public http: HttpClient) {
      this.titre = this.navParams.get("titre")
      this.file = this.navParams.get("file")
     
      // this.file="https://minhaparis.idevotion.fr/minhaparis/prierehtml/"+this.file
        this.file="assets/html/"+this.file
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.file);
       
          
      /*


        this.http.get(this.file,{responseType: 'text'})
          .subscribe(text => {
            this.priere= text;
            
          })
          
        this.apiService.showLoading()  
      let target="_self"
      this.iab.create(this.file,target,this.options)
      */
    }
  
  
  increase(){
    this.fontSize = this.fontSize * 1.1;
    console.log("font size "+this.fontSize)
    for (let elem in document.getElementsByTagName("framepriere")){
      console.log("------")
      console.log(elem)
    }
    let htmlRoot : HTMLElement = <HTMLElement> document.getElementsByTagName("framepriere")[0];
    if (htmlRoot != null) {
       
       console.log(htmlRoot)
       htmlRoot.style.fontSize = this.fontSize + '%';
       htmlRoot.style.color = 'red';
       //style.font-size]="fontSize + 'rem'"
       //document.getElementById("inneriframe").contentDocument.body.style.fontFamily = "Tahoma";
    }
    
  }

   
  decrease(){
    this.fontSize = this.fontSize / 1.1;
    if (this.fontSize<1.33){
      this.fontSize=1.33
    }
    console.log("font size "+this.fontSize)
   
    
  }
  
  dismissLoading() {
    this.apiService.stopLoading();
    
  }
  goBack(){
    this.navCtrl.pop()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowPrierePage');
  }

}
