import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FicheMinianPage } from './fiche-minian';

@NgModule({
  declarations: [
    FicheMinianPage,
  ],
  imports: [
    IonicPageModule.forChild(FicheMinianPage),
  ],
})
export class FicheMinianPageModule {}
