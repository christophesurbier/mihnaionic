import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Minha } from '../../models/minha';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Platform } from 'ionic-angular';

declare var google;


@IonicPage()
@Component({
  selector: 'page-fiche-minian',
  templateUrl: 'fiche-minian.html',
})
export class FicheMinianPage {
  minian: Minha;
  tabBarElement: any;
  latitude: number;
  longitude: number;
  data: any;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markers: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public apiService: ApiserviceProvider,
    public launchNavigator: LaunchNavigator,
    public platform: Platform) {
    this.minian = this.navParams.get("minian")
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }


  ionViewDidLoad() {
    this.loadMap();
  }

  goBack() {
    this.navCtrl.pop()
  }


  // Sets the map on all markers in the array.
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);

    }
  }

  // Removes the markers from the map, but keeps them in the array.
  clearMarkers() {
    this.setMapOnAll(null);
  }


  // Deletes all markers in the array by removing references to them.
  deleteMarkers() {
    console.log("deleteMarkers")
    this.clearMarkers();
    this.markers = [];

  }

  loadMap() {



    let currentUserLatitude = this.minian.latitude;
    let currentUserLongitude = this.minian.longitude;


    let latLng = new google.maps.LatLng(currentUserLatitude, currentUserLongitude);

    let mapOptions = {
      center: latLng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.refreshMarkers()


  }

  refreshMarkers() {
    if (this.markers == undefined) {
      this.markers = []
    }
    else {
      this.deleteMarkers()
    }
    this.addMarker()

  }

  addMarker() {
    let currentLatitude = this.minian.latitude;
    let currentLongitude = this.minian.longitude;

    let latLng = new google.maps.LatLng(currentLatitude, currentLongitude);
    var pinColor = "FCE041";
    //CHANGE FOR HTTPS IN PWA
    var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
      new google.maps.Size(21, 34),
      new google.maps.Point(0, 0),
      new google.maps.Point(10, 34));
    let marker = new google.maps.Marker({
      map: this.map,
      icon: pinImage,
      animation: google.maps.Animation.DROP,
      position: latLng
    });

    this.markers.push(marker)
    var contentString = '<div id="' + this.minian.id + '">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h4 id="firstHeading" class="firstHeading">' + this.minian.nom + '</h4>' +
      '<div id="bodyContent">'+this.minian.horaire+
    '</div>' +
      '</div>';
    this.addInfoWindow(marker, this.minian, contentString);
  }

  addInfoWindow(marker, place, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });


  }


  voirItineraire() {
    let latitude = this.minian.latitude;
    let longitude = this.minian.longitude;
    console.log("on lance navigateur ")
    this.launchNavigator.navigate([latitude, longitude]);
  }


}
