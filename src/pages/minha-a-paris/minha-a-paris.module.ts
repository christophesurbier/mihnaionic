import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhaAParisPage } from './minha-a-paris';
import {MomentModule} from 'angular2-moment';

@NgModule({
  declarations: [
    MinhaAParisPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhaAParisPage),
    MomentModule,
  ],
})
export class MinhaAParisPageModule {}
