import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import * as papa from 'papaparse';
import * as moment from 'moment';
import { EmailComposer } from '@ionic-native/email-composer';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-minha-a-paris',
  templateUrl: 'minha-a-paris.html',
})
export class MinhaAParisPage {
  doitAfficher = true;
  heureGauche = ""
  motToDisplay = ""
  heureDroite = ""
  latitude: any;
  longitude: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http,
    public emailComposer: EmailComposer,
    public platform: Platform,
    public geolocation: Geolocation,
    public apiService: ApiserviceProvider,
    public locac: LocationAccuracy) {
    this.readCsvData()
  }


  private readCsvData() {

    this.http.get('assets/chabbat2013.csv')
      .subscribe(
        data => this.extractData(data),
        err => {
          console.log("Err ")
        }
      );
  }

  private extractData(res) {
    let csvData = res['_body'] || '';
    let parsedData = papa.parse(csvData, {
      dynamicTyping: true
    }).data;

    // get week day 
    moment.locale("fr");
    let now = moment()
    // Lundi =1 , dimanche = 7
    let weekDay = now.isoWeekday()
    console.log("iso week day " + weekDay);
    if (weekDay >= 1 && weekDay <= 7) {

      // recupere heure
      let currentHeure = now.hour();
      console.log("Heure " + currentHeure)
      // on affiche du samedi 16h au samedi 15h
      /*
      if (weekDay == 7) {
        if (currentHeure < 1) {
          this.doitAfficher = false
        }
        else {
          this.doitAfficher = true
        }
      }
      else */
      if (weekDay == 6) {
        if (currentHeure >= 15 && currentHeure < 16) {
          this.doitAfficher = false
        }
        else {
          this.doitAfficher = true
        }
      }
      else {
        this.doitAfficher = true;
      }
      console.log("doit afficher " + this.doitAfficher)
      if (this.doitAfficher) {

        let numeroSemaine = now.isoWeek()
        console.log("semaine en cours " + numeroSemaine)
        if (weekDay == 6) {
          if (currentHeure >= 16) {
            numeroSemaine += 1
          }
        }
        else if (weekDay == 7) {
          numeroSemaine += 1
        }
        let currentYear = now.year()
        parsedData.forEach((element, idx) => {
          //console.log("on a "+element)
          let annee = element[0]
          let semaine = element[1]
          let heure = element[2]
          let mot = element[3]
          let fin = element[4]
          if (annee == currentYear && semaine == numeroSemaine) {
            console.log(annee + " " + semaine + " " + heure + " " + mot + " " + fin)
            this.heureGauche = heure
            this.motToDisplay = mot
            this.heureDroite = fin

          }
        })

      }

    }

  }
  openMinhaAParisMessagePage() {
    if (this.platform.is('cordova')) {
      let email = {
        to: 'info@horairesdesarcelles.com',
        subject: 'Information,aide ou suggestion (1.0.7)',
        body: '',
        isHtml: true
      };

      // Send a text message using default options
      this.emailComposer.open(email);
     
    }
    else {
      console.log("on est web")

      let email = 'info@horairesdesarcelles.com';
      let sub = "Information,aide ou suggestion (1.0.7)"

      var mailto_link = 'mailto:' + email + '?subject=' + encodeURIComponent(sub);

      // window.open(mailto_link, '_system');
      window.location.assign(mailto_link);
      // window.open(`mailto:${email}?Subject=toto`, '_system');
    }


  }



  openAutour() {
    this.geoloc().subscribe((done)=>{
      if (done){
        this.navCtrl.push("AroundPage", {
          latitude: this.latitude,
          longitude: this.longitude
        })
      }
    })
  }

  openVilleRP() {
    this.navCtrl.push("VillePage",{
      onlyRP:true
    })
  }

  openVilleFR() {
    this.navCtrl.push("VillePage",{
      onlyRP:false
    })
  }

  openHeure() {
    this.geoloc().subscribe((done)=>{
      if (done){
        this.navCtrl.push("AroundPage", {
          latitude: this.latitude,
          longitude: this.longitude,
          heure:true
        })
      }
    })
  }

  geoloc() {
    return Observable.create(observer => {
      if (this.apiService.networkConnected) {
        console.log("------ CHECK PLATFORM");
        if (this.platform.is('cordova')) {
          let options = {
            enableHighAccuracy: true
          };
          console.log("------  PLATFORM CORDOVA");
          this.apiService.showLoading();
          this.locac.request(this.locac.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
              this.apiService.stopLoading();
              console.log("============= POSITION " + JSON.stringify(position.coords) + "================");
              this.latitude = position.coords.latitude;
              this.longitude = position.coords.longitude;
              observer.next(true);
              observer.complete();
            }).catch((err) => {
              this.apiService.stopLoading();
              this.apiService.showError("Impossible de vous géolocaliser. Désolé mais la géolocalisation est indispensable pour voir les minians autour de vous");
              observer.next(false);
              observer.complete();
            })
          });
        }
        else {
          // webgeoloc
          if (navigator.geolocation) {
            this.apiService.showLoading();
            var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };
            
            navigator.geolocation.getCurrentPosition(
              position => {
                this.apiService.stopLoading();
                if (position.coords){
                  this.latitude = position.coords.latitude;
                  this.longitude = position.coords.longitude;
                  console.log("============= Latitude " + this.latitude + "================");
                  console.log("============= Longitudee " + this.longitude + "================");
                  observer.next(true);
                  observer.complete();
                }
                else{
                  this.apiService.showError("Impossible de vous géolocaliser. Désolé mais la géolocalisation est indispensable pour voir les minians autour de vous");
               
                  observer.next(false)
                  observer.complete()
                }
              },
              error => {
                this.apiService.stopLoading();
                observer.next(false);
                observer.complete();
                this.apiService.showError("Impossible de vous géolocaliser. Désolé mais la géolocalisation est indispensable pour voir les minians autour de vous");
                console.log("Error " + JSON.stringify(error));
                switch (error.code) {
                  case 1:
                    console.log('Permission Denied');
                    break;
                  case 2:
                    console.log('Position Unavailable');
                    break;
                  case 3:
                    console.log('Timeout');
                    break;
                }
              },
              options
            );
          }
        }
      }
      else {
        this.apiService.showNoNetwork();
        observer.next(false);
        observer.complete();
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MinhaAParisPage');
  }

}
