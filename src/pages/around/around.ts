import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Minha } from '../../models/minha';
import * as linkify from 'linkifyjs';;
import linkifyHtml from 'linkifyjs/html';
import hashtag from 'linkifyjs/plugins/hashtag'; // optional
import { min } from 'rxjs/operators';
const extractor = require('phone-number-extractor');

hashtag(linkify);

/**
 * Generated class for the AroundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-around',
  templateUrl: 'around.html',
})
export class AroundPage {
  tabBarElement: any;
  latitude : number;
  longitude:number;
  data : any;
  contactLink : string;
  debut : string;
  fin : string;
  ville:string;
  orderByHeure : boolean = false;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
  public apiService : ApiserviceProvider) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    this.latitude = navParams.get("latitude")
    this.longitude = navParams.get("longitude")
    this.ville = navParams.get("ville")
    let heure = navParams.get("heure")
    if (heure){
      this.orderByHeure = true;
    }
    this.loadMinian()
  }

  goBack(){
    this.navCtrl.pop()
  }

  goMap(){
    this.navCtrl.push("AroundMapPage",{
      latitude : this.latitude,
      longitude : this.longitude,
      data: this.data
    })
  }
  cliqueContact(e){
    e.stopPropagation();  
  }

  voirCarte(e,minha){
    e.stopPropagation(); 
    this.navCtrl.push('FicheMinianPage', {
      minian: minha
    }); 
  }
  loadMinian(){
    if (this.apiService.networkConnected){
      this.apiService.showLoading()
      this.apiService.getAroundJson(this.latitude,this.longitude,this.ville,this.orderByHeure).subscribe((result:any)=>{
        this.apiService.stopLoading()
        if (result){
          this.data = [];
          result.forEach((element,index) => {
           let minian = new Minha().initWithJSON(element);
           minian.horaire = minian.horaire.substring(0, 5);
           minian.horaire = minian.horaire.replace(":", "h");
           
           if (!this.debut){
            this.debut = minian.debut;
            this.fin = minian.fin;
           }
           minian.contactLink =  linkifyHtml(minian.contact, {
            defaultProtocol: 'https'
            });
           
            let arrayContact = minian.contactLink.split(";")
            let newTab = []
            if (arrayContact){
              for (let phrase of arrayContact){
               // console.log("phrase en entree "+phrase)
                  
                let phonenumber = phrase.replace( /^\D+/g, ''); // replace all leading non-digits with nothing
                if (phonenumber){
                 // console.log("y a numero "+phonenumber+" lenght "+phonenumber.length)
                  if (phonenumber.length<15 && phonenumber.length>9){
                   // console.log("on doit remplacer ")
                    let toReplace = "<a href=\"tel:"+phonenumber+"\">"+phonenumber+"</a>"
                    phrase = phrase.replace(phonenumber,toReplace)
                   // console.log("phrase remplace "+phrase)
                  
                  }
                }
                newTab.push(phrase)
              }
              //Change contactLink
              minian.contactLink=""
              for (let phrase of newTab){
                minian.contactLink+=phrase+";"
              }
              //Remove last ;
              minian.contactLink =  minian.contactLink.substring(0,  minian.contactLink.length - 1);
            }
           
           
            if (minian){
             this.data.push(minian)
           }
          });
         
        }
        else {
          this.apiService.showError("Désolé, nous n'avons pas réussi à récuperer les données. Merci d'essayer plus tard")
        }
      })
    }
    else {
      this.apiService.showNoNetwork()
    }
  }
  ionViewWillEnter() {
    if (this.tabBarElement){
      this.tabBarElement.style.display = 'none';
    }
  }
 
  ionViewWillLeave() {
    if (this.tabBarElement){
      this.tabBarElement.style.display = 'flex';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MinhaAParisMessagePage');
  }

}
