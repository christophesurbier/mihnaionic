import { Component } from '@angular/core';
import { Platform,AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase'; 
import { ApiserviceProvider } from '../providers/apiservice/apiservice';
import { Network } from '@ionic-native/network';

import { FCM } from '@ionic-native/fcm';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = "TabsPage";
  networkConnected : boolean = true;
  tokenEnvoiEnCours : boolean = false;

  constructor(
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public fcm: FCM,
  public apiService : ApiserviceProvider,
  public network : Network,
public badge:Badge,
public platform : Platform,
public alertCtrl:AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
     // splashScreen.hide();

      firebase.initializeApp({ 
        apiKey: "AIzaSyC1f_I7NBaQWLxc-2jLWFSPHHc7kV7TUcs", 
        authDomain: "minha-a-paris.firebaseapp.com", 
        databaseURL: "https://minha-a-paris.firebaseio.com", 
        projectId: "minha-a-paris", 
        storageBucket: "minha-a-paris.appspot.com", 
        messagingSenderId: "42911558941", 
      }); 
      this.initializeApp();
    });
  }


  watchNetwork() {
    console.log("================= WATCH NETWORK APP COMPONENT "+this.apiService.networkConnected+" ? ============")
    this.apiService.networkConnected = true
    this.networkConnected = true;
    // watch network for a connection
    this.network.onConnect().subscribe(() => {
      console.log('=====>APP COMPONENT network connect :-(');
      this.apiService.networkConnected = true
      this.networkConnected = true;
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
          this.apiService.networkConnected = true
          this.networkConnected = true;
        }
      }, 3000);
    });

    // watch network for a disconnect
    this.network.onDisconnect().subscribe(() => {
      console.log('=====>APP COMPONENT network was disconnected :-(');
      this.apiService.networkConnected = false;
      this.networkConnected = false;
    });
  }

  hideSplashscreen(){
    setTimeout(()=>{
        this.splashScreen.hide();
    },200)
  }


  initializeApp() {
    this.badge.clear();
      this.initPushNotification();
      this.hideSplashscreen();
    //Subscribe on resume
    this.platform.resume.subscribe(() => {
        this.badge.clear();
        // Check network at launch
        this.watchNetwork();
        this.apiService.isOnline = true;
        this.initPushNotification();
    });

    // Quit app
    this.platform.pause.subscribe(()=>{
      console.log("on RETOURNE en BACKGROUND");
        this.apiService.isOnline = false;
     });
  }

  
  showMessage(title,body){
    let confirmAlert = this.alertCtrl.create({
      title: title,
      message: body,
      buttons: [{
        text: 'Ok',
        role: 'cancel'
      }]
    });
    confirmAlert.present();
  }

  registerToken(registrationId){
    if (registrationId){
      let deviceName = "";
      let typeDevice = "";
      if (this.platform.is('android')) {
        deviceName = "android";
        typeDevice = "android";
      }
      else if (this.platform.is('ios')) {
        deviceName = "ios";
        typeDevice = "ios";
      }
      else {
        deviceName = "web";
        typeDevice = "web";
      }
      console.log("Envoi token ?"+this.tokenEnvoiEnCours)
      if (this.tokenEnvoiEnCours==false){
        this.tokenEnvoiEnCours = true
       
        this.apiService.sendFcmToken(registrationId,deviceName,typeDevice).subscribe((success)=>{
          this.tokenEnvoiEnCours = false
          if (!success){
            console.log("ERREUR ENVOI TOKEN");
            //TODO 
          }
          else {
            console.log("Envoi token serveur OK")
          }
        })


      }
      else {
        console.log("--- ENVOI TOKEN DEVICE DEJA EN COURS");
      }
    }
    else {
      console.log("====== ERREUR TOKEN RECU VIDE ====");
    }
  }

  initPushNotification() {

    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }

    this.fcm.getToken().then(token=>{
      console.log("---- GET TOKEN "+token);
      if (token){
        this.registerToken(token);
      }
    })


    this.fcm.onNotification().subscribe(notification=>{
      console.log("Notification Received  "+JSON.stringify(notification));
      this.badge.clear();
     
      if (this.platform.is('ios')) {
           let wasTapped = notification["wasTapped"];
           console.log("Was Tapped ? "+wasTapped);
           if (wasTapped){
             //Déja vue
           }
           else {
             let aps = notification["aps"];
              let alert = aps["alert"];
              this.showMessage(alert["title"],alert["body"]);
            }
      }
      else {
          if (notification.wasTapped){
            // Vient du background
          }
          else {
            if (notification["body"]){
              if (notification["body"].length>0){
                let confirmAlert = this.alertCtrl.create({
                  title: notification["title"],
                  message: notification["body"],
                  buttons: [{
                    text: 'Ok',
                    role: 'cancel'
                  }]
                });
                confirmAlert.present();
              }
            }
            
          }
        }
  
    });

    this.fcm.onTokenRefresh().subscribe(token=>{
      console.log("---- REFRESH TOKEN "+token);
      if (token){
        this.registerToken(token);
      }
    })

  }
}
