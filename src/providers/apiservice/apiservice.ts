import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, Loading } from 'ionic-angular';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/timeout';
import * as Constant from '../../config/constants';

/*
  Generated class for the ApiserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiserviceProvider {
  tokenSSO : String = "";
  networkConnected : boolean = true
  loader: any;
  expireDate: any;
  cordovaiOS = false;
  isOnline=false;
  // Backend URLS
  virtualHostName: String = Constant.domainConfig.virtual_host;
  getAroundMinhaUrl = this.virtualHostName+"/minhaparis/aroundMinhaJson.php";
  getFcmUrl = this.virtualHostName+"/minhaparis/insertAndroidToken.php"; 
  getVilleUrl = this.virtualHostName+"/minhaparis/getVilleJson.php";
 
  constructor(public http: HttpClient,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events) {

    console.log('--------- Hello ApiserviceProvider Provider');
    this.http = http
    this.storage = storage
    }
  


  showLoading() {
    if (this.loader){
      console.log("déjà une popup en cours on l'arrete pour en démarrer une autre")
      return
      /*
      this.loader.dismiss();
      this.loader = null;
      */
    }

    this.loader = this.loadingCtrl.create({
      content: 'Merci de patienter',
      dismissOnPageChange: false,
      duration:9000
    });
    this.loader.present();
  }


  stopLoading() {
    if (this.loader) {
      this.loader.dismiss();
      this.loader = null;
    }
  }

  showNoNetwork() {
    let alert = this.alertCtrl.create({
      title: 'Désolé',
      subTitle: 'Pas de réseau détecté. Merci de vérifier votre connexion 3G/4G ou Wifi',
      buttons: ['OK']
    });
    alert.present();
    //Reverifie la connexion
    this.events.publish('checkNetwork');
  }

  showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Erreur',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  showMessage(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }


  getAroundJson(latitude,longitude,ville,orderByHeure) {
    let body =""
    if (!ville){
      body = '?lat=' + latitude+"&lon="+longitude;
      if (orderByHeure){
        body+="&heure=oui"
      }
    }
    else{
      body='?ville='+ville
    }
    let url = this.getAroundMinhaUrl+body;
    console.log("on appelle BACKEND "+url);
    return this.http.get(url,{
        headers: new HttpHeaders().set('Authorization', 'oAuth: '+this.tokenSSO)
      })
  }



  getVilleJson(onlyRP) {
    let param=1
    if (!onlyRP){
      param=0;
    }
    let body = '?onlyRP=' + param;
    let url = this.getVilleUrl+body;
    console.log("on appelle BACKEND "+url);
    return this.http.get(url,{
        headers: new HttpHeaders().set('Authorization', 'oAuth: '+this.tokenSSO)
      })
  }


  sendFcmToken(tokenId,deviceName,typeDevice){
    let httpOptions = {
      headers: new HttpHeaders({
        'content-type': 'application/json',
        'Authorization': 'oAuth: '+this.tokenSSO
        })
      };
    let postParams = {
      "name": deviceName,
      "registration_id":tokenId,
      "device_id":tokenId,
      "active":true,
      "type":typeDevice
    };
    console.log("on envoi "+JSON.stringify(postParams))
    return this.http.post(this.getFcmUrl,postParams,httpOptions)
  }

}