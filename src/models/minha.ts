/**
 * A generic model that our Master-Detail pages list, create, and delete.
 *
 * Change "Item" to the noun your app will use. For example, a "Contact," or a
 * "Customer," or a "Animal," or something like that.
 *
 * The Items service manages creating instances of Item, so go ahead and rename
 * that something that fits your app as well.
 */
export class Minha {
    id: number;
    nom: string;
    ville:string;
    distance:number;
    horaire:string;
    informations:string;
    latitude:any;
    longitude:any;
    rue:string;
    office:string;
    jour:string;
    contact:string;
    contactLink:string;
    debut:string;
    fin:string;

    constructor() {
    }
  
    initWithJSON(json) : Minha{
      for (var key in json) {
          this[key] = json[key];
      }
      return this;
    }
}
