#!/bin/bash
FROM=windows-1252
TO=UTF-8
ICONV="iconv -f $FROM -t $TO"
# Convert
find . -type f -name "*.htm" | while read fn; do
cp ${fn} ${fn}.bak
$ICONV < ${fn}.bak > ${fn}
done
